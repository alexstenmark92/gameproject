class Weapon:
    def __init__(self):
        self.name = None
        raise NotImplementedError("Don't create raw a Weapon object.")

    def __str__(self):
        return self.name


class Rock(Weapon):
    def __init__(self):
        self.name = 'Rock'
        self.description = 'A fist-sized rock, well suited for bludgeoning.'
        self.damage = 5
        self.value = 1


class Dagger(Weapon):
    def __init__(self):
        self.name = 'Dagger'
        self.description = 'A fist-sized rock, well suited for bludgeoning.'
        self.damage = 10
        self.value = 30


class Katana(Weapon):
    def __init__(self):
        self.name = 'Katana'
        self.description = 'A fist-sized rock, well suited for bludgeoning.'
        self.damage = 20
        self.value = 100


class Greatsword(Weapon):
    def __init__(self):
        self.name = 'Greatsword'
        self.description = 'A fist-sized rock, well suited for bludgeoning.'
        self.damage = 30
        self.value = 150


class Consumable:
    def __init__(self):
        self.name = None
        self.healing_value = None
        raise NotImplementedError("Don't create a raw consumable object")

    def __str__(self):
        return f'{self.name} (+ {self.healing_value})'


class LoafBread(Consumable):
    def __init__(self):
        self.name = 'Loaf of Bread'
        self.healing_value = 10
        self.value = 12


class HealingPotion(Consumable):
    def __init__(self):
        self.name = 'Healing Potion'
        self.healing_value = 50
        self.value = 75
