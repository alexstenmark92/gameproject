import random
import enemies
import npc
import player


class MapTile:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def intro_text(self):
        raise NotImplementedError ('Dont create a raw maptile object')

    def modify_player(self, player):
        pass


class StartTile(MapTile):
    def intro_text(self):
        return '''
        This is where you start.
        '''


class VictoryTile(MapTile):

    def modify_player(self):
        player.victory = True

    def intro_text(self):
        return '''
        You have won you passed the forest.
        '''


class EnemyTile(MapTile):

    def __init__(self, x, y):
        r = random.random()
        if r < 0.50:
            self.enemy = enemies.GiantSpider()
            self.alive_text = 'Living Spider'
            self.dead_text = 'Dead Spider'
        elif r < 0.80:
            self.enemy = enemies.Ogre()
            self.alive_text = 'Living Ogre'
            self.dead_text = 'Dead Ogre'
        elif r < 0.95:
            self.enemy = enemies.BatColony()
            self.alive_text = 'Living Bats'
            self.dead_text = 'Dead Bats'
        else:
            self.enemy = enemies.Golem()
            self.alive_text = 'Living Golem'
            self.dead_text = 'Dead Golem'

        super().__init__(x, y)

    def intro_text(self):
        if self.enemy.is_alive():
            return f'{self.alive_text}'
        else:
            return f'{self.dead_text}'

    def modify_player(self, player):
        if self.enemy.is_alive():
            damage = self.enemy.damage - player.armor
            if damage > 0:
                player.hp = player.hp - damage
                print(f'Enemy does {damage}, You now have {player.hp} HP left.')
            else:
                print(f'{self.enemy.name} failed to damage you through your armor.')


class TraderTile(MapTile):
    def __init__(self, x, y):
        self.trader = npc.Trader()
        super().__init__(x, y)

    def trade(self, buyer, seller):
        for i, item in enumerate(seller.inventory, 1):
            print(f'{i}. {item.name} - {item.value} Gold')
        while True:
            user_input = input('Choose an item or press Q to exit: ')
            if user_input in ['Q', 'q']:
                return
            else:
                try: 
                    choice = int(user_input)
                    to_swap = seller.inventory[choice - 1]
                    self.swap(seller, buyer, to_swap)
                except ValueError:
                    print('Not a valid choice.')

    def check_if_trade(self, player):
        while True:
            print('Would you like to (B)uy, (S)ell or (Q)uit?')
            user_input = input()
            if user_input in ['Q', 'q']:
                return
            elif user_input in ('B', 'b'):
                print("Here's whats available to buy: ")
                self.trade(buyer=player, seller=self.trader)
            elif user_input in('S', 's'):
                print("Here's whatss available to sell: ")
                self.trade(buyer=self.trader, seller=player)
            else:
                print('Not a valid choice.')

    def intro_text(self):
        return 'placeholder text trader'
    
    def swap(self, seller, buyer, item):
        if item.value > buyer.gold:
            print("That's too expensive")
            return
        seller.inventory.remove(item)
        buyer.inventory.append(item)
        seller.gold = seller.gold + item.value
        buyer.gold = buyer.gold - item.value
        print('Trade Complete.')


class FindGoldTile(MapTile):
    def __init__(self, x, y):
        self.gold = random.randint(1, 50)
        self.gold_claimed = False
        super().__init__(x, y)
    
    def modify_player(self, player):
        if not self.gold_claimed:
            self.gold_claimed = True
            player.gold = player.gold + self.gold
            print(f'+ {self.gold} gold added.')
    
    def intro_text(self):
        if self.gold_claimed:
            return '''
            There is no gold left. Placeholder text.
            '''
        else:
            return ''' Theres a chest filled with gold here, placeholder text'''

# world_map = [
#     [None,VictoryTile(1,0),None],
#     [None,EnemyTile(1,1),None],
#     [EnemyTile(0,2),StartTile(1,2),EnemyTile(2,2)],
#     [None,EnemyTile(1,3),None]
# ]


world_map = []

# world_dsl = '''
# |  |VT|  |
# |  |EN|  |
# |EN|ST|EN|
# |  |EN|  |
# '''


world_dsl = '''
|EN|  |TT|  |VT|
|EN|FG|EN|  |EN|
|EN|  |EN|EN|EN|
|ST|FG|  |  |EN|
|EN|EN|TT|  |FG|
'''
tile_type_dict = {'VT': VictoryTile,
                  'EN': EnemyTile,
                  'ST': StartTile,
                  'TT': TraderTile,
                  'FG': FindGoldTile,
                  '  ': None}


def is_dsl_valid(dsl):
    if dsl.count('|ST|') != 1:
        return False
    if dsl.count('|VT|') == 0:
        return False
    lines = dsl.splitlines()
    lines = [l for l in lines if l]
    pipe_counts = [line.count('|') for line in lines]
    for count in pipe_counts:
        if count != pipe_counts[0]:
            return False
    return True


def parse_world_dsl():
    if not is_dsl_valid(world_dsl):
        raise SyntaxError('Error in world map')
    dsl_lines = world_dsl.splitlines()
    dsl_lines = [x for x in dsl_lines if x]
    for y, dsl_row in enumerate(dsl_lines):
        row = []
        dsl_cells = dsl_row.split('|')
        dsl_cells = [c for c in dsl_cells if c]
        for x, dsl_cell in enumerate(dsl_cells):
            tile_type = tile_type_dict[dsl_cell]
            if tile_type == StartTile:
                global start_tile_location
                start_tile_location = x, y
            row.append(tile_type(x, y) if tile_type else None)
        world_map.append(row)


def tile_at(x, y):
    if x < 0 or y < 0:
        return None
    try:
        return world_map[y][x]
    except IndexError:
        return None
